package edu.upenn.cis.cis455;

import static spark.Spark.*;

public class CalcService {
    public static void main(String[] args) {
    	port(45555);

        get("/hello", (req, res) -> "Hello World");

        System.out.println("Waiting to handle requests!");
        awaitInitialization();
    }
}
